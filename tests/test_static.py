
import os
import re
import shutil
from string import ascii_lowercase
import sys
import time
import unittest

try:
    from unittest import mock
except ImportError:
    import mock

from staticky.utils import NS, HAVE_NS_MTIME

DATA = os.path.join(os.path.dirname(__file__), "data")

try:
    FileNotFoundError
except NameError: # python<3.3
    FileNotFoundError = OSError

def time_ns(inc=0):
    return int(time.time()*NS)+inc*NS

class Rot13Rule(object):
    def __init__(self):
        letters = ascii_lowercase
        letters_shifted = ascii_lowercase[13:] + ascii_lowercase[:13]

        try:
            maketrans = "".maketrans
        except AttributeError: # python2.7
            from string import maketrans

        self.tmap = maketrans(letters + letters.upper(),
                                 letters_shifted + letters_shifted.upper())

    def __call__(self, ctx, input, output):
        with ctx.open(input, "r") as fin:
            with ctx.open(output, "w") as fout:
                fout.write(fin.read().translate(self.tmap))

class DepRule(object):
    def __call__(self, ctx, input, output):
        with ctx.open(input, "r") as f:
            path = f.readline().strip()

        with ctx.open(path, "r") as fin:
            with ctx.open(output, "w") as fout:
                fout.write(fin.read())

class CatDirRule(object):
    def __init__(self, recursive=False):
        self.recursive = recursive

    def cat_dir(self, path, fout):
        for p in sorted(os.listdir(path)):
            subpath = os.path.join(path, p)
            if os.path.isdir(subpath):
                if self.recursive:
                    self.cat_dir(subpath, fout)
            else:
                with open(subpath, "rb") as fin:
                    shutil.copyfileobj(fin, fout)

    def __call__(self, ctx, input, output):
        with ctx.open(input, "r") as f:
            path = f.readline().strip()

        with ctx.open(output, "wb") as fout:
            self.cat_dir(ctx.resolve(path), fout)

class BrokenRule(object):
    def __call__(self, ctx, input, output):
        raise ValueError("This rule is useless!")

class LimitRunRule(object):
    def __init__(self, count=1):
        self.remaining = count

    def __call__(self, ctx, input, output):
        assert self.remaining > 0
        self.remaining -= 1

        with ctx.open(input, "r") as fin:
            with ctx.open(output, "w") as fout:
                shutil.copyfileobj(fin, fout)

class NoWhiteSpaceRule(object):
    def __call__(self, ctx, name):
        with ctx.open(name, "rb") as f:
            text = re.sub(r'\s+'.encode('utf-8'), b'', f.read())

        with ctx.open(name, "wb") as f:
            f.write(text)

class call_application(object):
    def __init__(self, app, req):
        self.app = app
        self.req = req

    def __enter__(self):
        status, headers, self.body_iter = self.req.call_application(self.app)
        return status, headers, self.body_iter

    def __exit__(self, type, val, tb):
        try:
            self.body_iter.close()
        except:
            pass

class TestsBase(object):
    def assertEqualsNL(self, str1, str2):
        self.assertEquals(str1.splitlines(), str2.splitlines())

    def assertContentsText(self, path, contents):
        with self.staticfiles.open(path, "r") as t:
            self.assertEqualsNL(t.read(), contents)

    def assertContents(self, path, contents):
        with self.staticfiles.open('/'+path, "rb") as t:
            self.assertEqualsNL(t.read(), contents)

        from webob import Request
        req = Request.blank('/'+path)
        with call_application(self.staticfiles.as_wsgi, req) as (status, headers, resp):
            self.assertEqualsNL(b"".join(resp), contents)

    def assertNoExist(self, path):
        with self.assertRaises(FileNotFoundError):
            with self.staticfiles.open(path, "rb"):
                pass

        from webob import Request
        req = Request.blank('/'+path)
        with call_application(self.staticfiles.as_wsgi, req) as (status, headers, resp):
            self.assertRegexpMatches(status, r'^404 ')

    def assertInstalls(self, filelist, contents=(), gzip_contents=(), **kwargs):
        from staticky.utils import walk_tree, native_to_url_path
        import gzip
        import tempfile

        tmpdir = tempfile.mkdtemp()

        try:
            self.staticfiles.install(tmpdir, **kwargs)

            self.assertEquals(
                sorted(filelist),
                sorted(native_to_url_path(i.path) for i in walk_tree(tmpdir))
            )

            for path, data in contents:
                with open(os.path.join(tmpdir, path), "rb") as f:
                    self.assertEqualsNL(f.read(), data)

            for path, data in gzip_contents:
                with gzip.GzipFile(os.path.join(tmpdir, path)) as f:
                    self.assertEqualsNL(f.read(), data)
        finally:
            shutil.rmtree(tmpdir)

    def createFiles(self, root, paths, mtime=None):
        from staticky.utils import mkdir_p, touch_file

        for path in paths:
            if isinstance(path, tuple):
                path, contents = path
            else:
                contents = b''

            fullpath = os.path.join(root, path)
            mkdir_p(os.path.dirname(fullpath))
            with open(fullpath, "wb") as f:
                f.write(contents)

            if mtime is not None:
                touch_file(fullpath, mtime)

    def deleteFiles(self, root, paths):
        for path in paths:
            fullpath = os.path.join(root, path)
            if os.path.isdir(fullpath):
                shutil.rmtree(fullpath)
            else:
                os.unlink(fullpath)

#
# Basic Tests
#

class BasicTestsBase(TestsBase):
    def setUp(self):
        self.staticfiles.register(Rot13Rule, input="{name}.txt", output="{name}.rot13")

    def tearDown(self):
        if hasattr(self, "staticfiles"):
            self.staticfiles.cleanup()
            self.staticfiles = None

    def test_basic_open(self):
        # binary
        with self.staticfiles.open("test1.txt", "rb") as f:
            self.assertEquals(f.read(), b"Hello, world!\n")

        with self.staticfiles.open("test1.txt", "rb") as f:
            self.assertEquals(f.readline(), b"Hello, world!\n")

        with self.staticfiles.open("test1.txt", "rb") as f:
            self.assertEquals(next(iter(f)), b"Hello, world!\n")

        # text
        with self.staticfiles.open("test1.txt", "r") as f:
            self.assertEquals(f.read(), "Hello, world!\n")

        with self.staticfiles.open("test1.txt", "r") as f:
            self.assertEquals(f.readline(), "Hello, world!\n")

        with self.staticfiles.open("test1.txt", "r") as f:
            self.assertEquals(next(iter(f)), "Hello, world!\n")

    def test_basic(self):
        self.assertContents("test1.txt", b"Hello, world!\n")
        self.assertContentsText("test1.txt", "Hello, world!\n")

    def test_rot13(self):
        self.assertContents("test1.rot13", b"Uryyb, jbeyq!\n")
        self.assertContentsText("test1.rot13", "Uryyb, jbeyq!\n")

    def test_override(self):
        self.assertContents("test2.txt", b"Greetings, World!\n")
        self.assertContents("test2.rot13", b"Terrgvatf, Jbeyq!\n")

    def test_wsgi_file_wrapper_basic(self):
        from webob import Request
        from wsgiref.util import FileWrapper
        def _app(environ, start_response):
            environ['wsgi.file_wrapper'] = FileWrapper
            return self.staticfiles.as_wsgi(environ, start_response)

        req = Request.blank('/test1.txt')
        with call_application(self.staticfiles.as_wsgi, req) as (status, headers, resp):
            self.assertEqualsNL(b"".join(resp), b"Hello, world!\n")

    def test_wsgi_file_wrapper_rot13(self):
        from webob import Request
        from wsgiref.util import FileWrapper
        def _app(environ, start_response):
            environ['wsgi.file_wrapper'] = FileWrapper
            return self.staticfiles.as_wsgi(environ, start_response)

        req = Request.blank('/test1.rot13')
        with call_application(_app, req) as (status, headers, resp):
            self.assertEqualsNL(b"".join(resp), b"Uryyb, jbeyq!\n")

    def test_install(self):
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test2.rot13', 'test2.txt'],
            [("test1.txt", b"Hello, world!\n"), ("test1.rot13", b"Uryyb, jbeyq!\n")]
        )

    def test_install_override(self):
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test2.rot13', 'test2.txt'],
            [("test2.txt", b"Greetings, World!\n"), ("test2.rot13", b"Terrgvatf, Jbeyq!\n")]
        )

    def test_basic_post(self):
        self.staticfiles.register_post(NoWhiteSpaceRule, name="{n}.txt")

        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test2.rot13', 'test2.txt'],
            [("test1.txt", b"Hello,world!"), ("test1.rot13", b"Uryyb, jbeyq!\n")]
        )

    def test_rule_exception(self):
        from staticky import RuleError
        from staticky.log import logger

        self.staticfiles.register(BrokenRule, input="{name}.txt", output="{name}.nope")

        with mock.patch.object(logger, "exception") as log_exception:
            self.assertInstalls(['', 'test1.rot13', 'test1.txt', 'test2.rot13', 'test2.txt'])

            assert log_exception.called

class BasicSourceOnlyTest(TestsBase, unittest.TestCase):
    def setUp(self):
        from staticky import StaticFiles
        self.staticfiles = StaticFiles(input=[os.path.join(DATA, "input1")])

    def test_basic(self):
        self.assertContents("test1.txt", b"Hello, world!\n")
        self.assertContentsText("test1.txt", "Hello, world!\n")

    def test_cycle_warning(self):
        import warnings
        from staticky import CycleWarning

        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")

            self.staticfiles.register(BrokenRule, input='{name}.txt', output='{name}.txt')

            # This isn't the point of the test, but we need to force
            # targets to be resolved.
            with self.assertRaises(FileNotFoundError):
                self.assertContents("test-nope.txt", b"")

            assert issubclass(w[-1].category, CycleWarning)

    def test_imported(self):
        from staticky import StaticFiles
        from tempfile import mkdtemp

        tmpdir = mkdtemp()
        try:
            with mock.patch("staticky.utils.lookup"):
                sf = StaticFiles(input=[os.path.join(DATA, "input1")])
                sf.register('fake', input='{name}.txt', output='{name}.fake')
                sf.install(tmpdir)
        finally:
            shutil.rmtree(tmpdir)

class BasicTestsNoOutputTest(BasicTestsBase, unittest.TestCase):
    def setUp(self):
        from staticky import StaticFiles
        self.staticfiles = StaticFiles(input=[os.path.join(DATA, "input1")])

        super(BasicTestsNoOutputTest, self).setUp()

class BasicTestsOutputTest(BasicTestsBase, unittest.TestCase):
    def setUp(self):
        from tempfile import mkdtemp
        from staticky import StaticFiles

        self.tmpdir = mkdtemp()
        self.staticfiles = StaticFiles(input=[os.path.join(DATA, "input1")], workdir=self.tmpdir)
        super(BasicTestsOutputTest, self).setUp()

    def tearDown(self):
        super(BasicTestsOutputTest, self).tearDown()
        if hasattr(self, "tmpdir"):
            shutil.rmtree(self.tmpdir)

class BasicTestsInput2Test(BasicTestsBase, unittest.TestCase):
    def setUp(self):
        from staticky import StaticFiles
        self.staticfiles = StaticFiles(input=[
            os.path.join(DATA, "input1"),
            os.path.join(DATA, "input2")
        ])

        super(BasicTestsInput2Test, self).setUp()

    def test_override(self):
        self.assertContents("test2.txt", b"Howdy, World!\n")
        self.assertContents("test2.rot13", b"Ubjql, Jbeyq!\n")

    def test_install_override(self):
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test2.rot13', 'test2.txt'],
            [("test2.txt", b"Howdy, World!\n"), ("test2.rot13", b"Ubjql, Jbeyq!\n")]
        )

    def test_install_gzip(self):
        self.assertInstalls(
            ['', 'test1.txt', 'test1.txt.gz', 'test1.rot13', 'test1.rot13.gz',
             'test2.rot13', 'test2.rot13.gz', 'test2.txt', 'test2.txt.gz'],
            gzip_contents = [("test2.txt.gz", b"Howdy, World!\n"), ("test2.rot13.gz", b"Ubjql, Jbeyq!\n")],
            gzip = True
        )

#
#
#

class DynamicTests(TestsBase, unittest.TestCase):
    def setUp(self):
        import tempfile

        self.workdir = None
        self.input = tempfile.mkdtemp()
        self.init_staticfiles()

    def init_staticfiles(self):
        import tempfile
        from staticky import StaticFiles

        if self.workdir is None:
            self.workdir = tempfile.mkdtemp()

        self.staticfiles = StaticFiles(input=[self.input], workdir=self.workdir)
        self.staticfiles.register(Rot13Rule, input="{name}.txt", output="{name}.rot13")

    def tearDown(self):
        shutil.rmtree(self.input)
        self.staticfiles.cleanup()

        if self.workdir is not None:
            shutil.rmtree(self.workdir)

    def test_absolute(self, install_args={}):
        self.staticfiles.register(Rot13Rule, input="/test-in/{name}.in", output="/test-out/{name}.out")
        self.createFiles(self.input, [('test-in/test1.in', b"data\n")])

        self.assertInstalls(
            ['', 'test-in', 'test-in/test1.in', 'test-out', 'test-out/test1.out'],
            [("test-in/test1.in", b"data\n"), ("test-out/test1.out", b"qngn\n")],
            **install_args
        )

    def test_deleted(self, install_args={}):
        self.createFiles(self.input, [('test1.txt', b"data\n")])
        self.assertContents("test1.txt", b"data\n")
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13'],
            [("test1.txt", b"data\n"), ("test1.rot13", b"qngn\n")],
            **install_args
        )

        self.deleteFiles(self.input, ['test1.txt'])
        self.assertInstalls([''], **install_args)
        self.assertNoExist('test1.txt')
        self.assertNoExist('test2.txt')

    def test_deleted_clean(self):
        return self.test_deleted(install_args={'clean': True})

    def test_replaced(self, install_args={}):
        self.createFiles(self.input, [('test1.txt', b"data\n")], mtime=time_ns())
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13'],
            [("test1.txt", b"data\n"), ("test1.rot13", b"qngn\n")],
            **install_args
        )

        self.createFiles(self.input, [('test1.txt', b"lore\n")], mtime=time_ns(1))
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13'],
            [("test1.txt", b"lore\n"), ("test1.rot13", b"yber\n")],
            **install_args
        )

    def test_replaced_clean(self):
        return self.test_replaced(install_args={'clean': True})

    def test_dir2file(self, install_args={}):
        self.createFiles(self.input, [
            ('sub/test1.txt', b"data-sd\n"),
            ('test2.txt', b"data\n")
        ], mtime=time_ns())
        self.assertInstalls(
            ['', 'sub', 'sub/test1.txt', 'sub/test1.rot13', 'test2.rot13', 'test2.txt'],
            [("sub/test1.txt", b"data-sd\n")],
            **install_args
        )

        self.deleteFiles(self.input, ['sub'])
        self.createFiles(self.input, [
            ('sub', b"data-sd2\n")
        ], mtime=time_ns(1))
        self.assertInstalls(
            ['', 'sub', 'test2.txt', 'test2.rot13'],
            [("sub", b"data-sd2\n")],
            **install_args
        )
        self.assertNoExist('sub/test1.txt')

    def test_dir2file_clean(self):
        return self.test_dir2file(install_args={'clean': True})

    def test_file2dir(self, install_args={}):
        self.createFiles(self.input, [
            ('sub', b"hello\n"),
            ('test2.txt', b"data\n")
        ], mtime=time_ns())
        self.assertInstalls(
            ['', 'sub', 'test2.txt', 'test2.rot13'],
            [("sub", b"hello\n")],
            **install_args
        )

        self.deleteFiles(self.input, ['sub'])
        self.createFiles(self.input, [
            ('sub/test1.txt', b"who you be\n")
        ], mtime=time_ns(1))
        self.assertInstalls(
            ['', 'sub', 'sub/test1.txt', 'sub/test1.rot13', 'test2.txt', 'test2.rot13'],
            [("sub/test1.rot13", b"jub lbh or\n")],
            **install_args
        )

    def test_file2dir_clean(self):
        return self.test_file2dir(install_args={'clean': True})

    def test_file_touched(self):
        self.createFiles(self.input, [
            ('test1.txt', b"data\n")
        ], mtime=time_ns())
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13'],
            [("test1.txt", b"data\n"), ("test1.rot13", b"qngn\n")]
        )

        self.createFiles(self.input, [
            ('test1.txt', b"lore\n")
        ], mtime=time_ns(1))
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13'],
            [("test1.rot13", b"yber\n")]
        )

    def test_file_touched_dep(self, reset=False):
        self.staticfiles.register(DepRule, input="{name}.in", output="{name}.out")

        self.createFiles(self.input, [
            ('test1.txt', b"alpha\n"),
            ('test2.txt', b"beta\n"),
            ('test.in', b"test1.txt")
        ], mtime=time_ns())
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test2.txt', 'test2.rot13', 'test.in', 'test.out'],
            [('test.out', b'alpha\n')]
        )

        if reset:
            self.init_staticfiles()
            self.staticfiles.register(DepRule, input="{name}.in", output="{name}.out")

        self.createFiles(self.input, [
            ('test.in', b"test2.txt")
        ], mtime=time_ns(1))
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test2.txt', 'test2.rot13', 'test.in', 'test.out'],
            [('test.out', b'beta\n')]
        )

    def test_file_touched_dep_reset(self):
        return self.test_file_touched_dep(reset=True)

    def test_file_dep_touched(self, reset=False):
        self.staticfiles.register(DepRule, input="{name}.in", output="{name}.out")

        self.createFiles(self.input, [
            ('test1.txt', b"alpha\n"),
            ('test.in', b"test1.txt")
        ], mtime=time_ns())
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test.in', 'test.out'],
            [('test.out', b'alpha\n')]
        )

        if reset:
            self.init_staticfiles()
            self.staticfiles.register(DepRule, input="{name}.in", output="{name}.out")

        self.createFiles(self.input, [
            ('test1.txt', b"beta\n")
        ], mtime=time_ns(1))
        self.assertInstalls(
            ['', 'test1.txt', 'test1.rot13', 'test.in', 'test.out'],
            [('test.out', b'beta\n')]
        )

    def test_file_dep_touched_reset(self):
        self.test_file_dep_touched(reset=True)

    def test_resolve_dir(self):
        self.staticfiles.register(CatDirRule, input="{name}.catdir", output="{name}.txt")

        self.createFiles(self.input, [
            ('f.catdir', b"dir\n"), ('dir/a', b"abc\n"), ('dir/b.txt', b"def\n"),
        ])
        self.assertInstalls(
            ['', 'f.catdir', 'f.txt', 'f.rot13', 'dir', 'dir/a', 'dir/b.txt', 'dir/b.rot13'],
            [('f.txt', b'abc\nqrs\ndef\n')]
        )

    def test_resolve_dir_dep_deleted(self):
        self.staticfiles.register(CatDirRule, input="{name}.catdir", output="{name}.txt")

        self.createFiles(self.input, [
            ('f.catdir', b"dir\n"), ('dir/a', b"abc\n"), ('dir/b.txt', b"def\n"),
        ])
        self.assertInstalls(
            ['', 'f.catdir', 'f.txt', 'f.rot13', 'dir', 'dir/a', 'dir/b.txt', 'dir/b.rot13'],
            [('f.txt', b'abc\nqrs\ndef\n')]
        )

        self.deleteFiles(self.input, ['dir/b.txt'])
        self.assertInstalls(
            ['', 'f.catdir', 'f.txt', 'f.rot13', 'dir', 'dir/a'],
            [('f.txt', b'abc\n')]
        )

    def test_resolve_dir_dep3_deleted(self):
        self.staticfiles.register(CatDirRule, input="{name}.catdir", output="{name}.txt", args={
            'recursive' : True
        })

        self.createFiles(self.input, [
            ('f.catdir', b"dir\n"), ('dir/a', b"abc\n"), ('dir/b/b/b.txt', b"def\n"),
        ])
        self.assertInstalls(
            ['', 'f.catdir', 'f.txt', 'f.rot13', 'dir', 'dir/a', 'dir/b',
             'dir/b/b', 'dir/b/b/b.txt', 'dir/b/b/b.rot13'],
            [('f.txt', b'abc\nqrs\ndef\n')]
        )

        self.deleteFiles(self.input, ['dir/b/b/b.txt'])
        self.assertInstalls(
            ['', 'f.catdir', 'f.txt', 'f.rot13', 'dir', 'dir/a'],
            [('f.txt', b'abc\n')]
        )

    def test_single_run(self):
        self.staticfiles.register(LimitRunRule, input="{name}.in", output="{name}.out")

        self.createFiles(self.input, [
            ('test1.in', b"alpha\n"),
        ])
        self.assertInstalls(
            ['', 'test1.in', 'test1.out'],
            [('test1.out', b'alpha\n')]
        )

    def test_single_run_dep1(self):
        self.staticfiles.register(LimitRunRule, input="{name}.a", output="{name}.b")
        self.staticfiles.register(LimitRunRule, input="{name}.b", output="{name}.c")

        self.createFiles(self.input, [
            ('test1.a', b"alpha\n"),
        ])
        self.assertInstalls(
            ['', 'test1.a', 'test1.b', 'test1.c'],
            [('test1.c', b'alpha\n')]
        )

    def test_limit_run_touch_dir(self):
        limit_rule = LimitRunRule(count=1)
        self.staticfiles.register(lambda: limit_rule, input="{name}.x", output="{name}.y")
        self.staticfiles.register(CatDirRule, input="{name}.catdir", output="{name}.out")

        self.createFiles(self.input, [
            ('f.catdir', b"dir\n"), ('dir/a.x', b"abc\n"), ('dir/b.y', b"def\n"),
        ],  mtime=time_ns())
        self.assertInstalls(
            ['', 'f.catdir', 'f.out', 'dir', 'dir/a.x', 'dir/a.y', 'dir/b.y'],
            [('f.out', b'abc\nabc\ndef\n')]
        )

        # reset
        limit_rule.remaining = 1

        self.createFiles(self.input, [
            ('dir/a.x', b"ghi\n")
        ],  mtime=time_ns(1))
        self.assertInstalls(
            ['', 'f.catdir', 'f.out', 'dir', 'dir/a.x', 'dir/a.y', 'dir/b.y'],
            [('f.out', b'ghi\nghi\ndef\n')]
        )
