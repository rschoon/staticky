
import os
import shutil
import sys
import tempfile
import unittest

try:
    from unittest import mock
except ImportError:
    import mock

try:
    from StringIO import StringIO # python2.7
except ImportError:
    from io import StringIO

from staticky.utils import NS, HAVE_NS_MTIME

#
#
#

class TempDirTest(unittest.TestCase):
    def setUp(self):
        self.dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.dir)

#
#
#

class ClosingIteratorTest(unittest.TestCase):
    def test_generator(self):
        from staticky.utils import closing_iterator

        def itr():
            try:
                yield ''
            finally:
                itr.finally_run = True
        itr.finally_run = False

        self.assertEquals(list(closing_iterator(itr())), [''])
        self.assertTrue(itr.finally_run)

    def test_filelike(self):
        from staticky.utils import closing_iterator

        class F(object):
            def __init__(self):
                self.close_called = False

            def __iter__(self):
                yield ''

            def close(self):
                self.close_called = True

        f = F()
        self.assertEquals(list(closing_iterator(f)), [''])
        self.assertTrue(f.close_called)

class GetMTimeRecursiveTest(TempDirTest):
    def _touch(self, path, t):
        from staticky.utils import touch_file

        if not os.path.exists(path):
            open(path, "w").close()

        touch_file(path, t)

    def assertEqualsNS(self, a, b):
        if sys.version_info < (3, 3):
            # utime ns is not available, only compare to second
            return self.assertEquals(int(a/NS), int(b/NS))
        return self.assertEquals(a, b)

    def test_mtime(self):
        from staticky.utils import mkdir_p, getmtime, getmtime_recursive

        base_mtime = getmtime(self.dir)
        self.assertEqualsNS(getmtime_recursive(self.dir), base_mtime)

        path1 = os.path.join(self.dir, "path")
        mkdir_p(path1)
        self._touch(path1, base_mtime+1*NS)
        self.assertEqualsNS(getmtime_recursive(self.dir), base_mtime+1*NS)

        path2 = os.path.join(self.dir, "path2")
        self._touch(path2, base_mtime+2*NS)
        self.assertEqualsNS(getmtime_recursive(self.dir), base_mtime+2*NS)
        self.assertEqualsNS(getmtime_recursive(path2), base_mtime+2*NS)

        self._touch(os.path.join(self.dir, "path", "subpath"), base_mtime+3*NS)
        self.assertEqualsNS(getmtime_recursive(self.dir), base_mtime+3*NS)

        self._touch(path1, base_mtime+4*NS)
        self.assertEqualsNS(getmtime_recursive(self.dir), base_mtime+4*NS)

        self._touch(self.dir, base_mtime+5*NS)
        self.assertEqualsNS(getmtime_recursive(self.dir), base_mtime+5*NS)

class InplaceTempFileTest(TempDirTest):
    def test_unnamed(self):
        from staticky.utils import InplaceTempFile

        path = os.path.join(self.dir, "output.txt")
        with InplaceTempFile(path) as f:
            f.write(b"hello!\n")

        with open(path, "rb") as f:
            self.assertEquals(f.read(), b"hello!\n")

    def test_named(self):
        from staticky.utils import InplaceTempFile

        path = os.path.join(self.dir, "output.txt")
        inp = InplaceTempFile(path, named=True)

        with inp as f:
            self.assertTrue(os.path.isfile(inp.tempfile.name))
            f.write(b"hello!\n")
        self.assertFalse(os.path.isfile(inp.tempfile.name))

        with open(path, "rb") as f:
            self.assertEquals(f.read(), b"hello!\n")

    def test_named_raised(self):
        from staticky.utils import InplaceTempFile

        path = os.path.join(self.dir, "output.txt")
        with open(path, "wb") as f:
            f.write(b"keepme\n")

        inp = InplaceTempFile(path, named=True)
        with self.assertRaises(RuntimeError):
            with inp as f:
                raise RuntimeError
        self.assertFalse(os.path.isfile(inp.tempfile.name))

        with open(path, "rb") as f:
            self.assertEquals(f.read(), b"keepme\n")

class IterFileChunksTest(unittest.TestCase):
    def test_stringio(self):
        from staticky.utils import iter_file_chunks
        self.assertEquals("".join(iter_file_chunks(StringIO("a"))), "a")

class LookupTest(unittest.TestCase):
    def test_lookup(self):
        from staticky import StaticFiles
        from staticky.utils import lookup

        def f():
            pass

        self.assertEquals(lookup("staticky:StaticFiles"), StaticFiles)
        self.assertEquals(lookup(f), f)

class MkdirTest(TempDirTest):
    def test_mkdir(self):
        from staticky.utils import mkdir_p

        mkdir_p(os.path.join(self.dir, "test"))
        self.assertTrue(os.path.isdir(os.path.join(self.dir, "test")))

        mkdir_p(os.path.join(self.dir, "test2", "test3"))
        mkdir_p(os.path.join(self.dir, "test2", "test3"))
        self.assertTrue(os.path.isdir(os.path.join(self.dir, "test2", "test3")))

class URLConversionTest(unittest.TestCase):
    def test_native_to_url(self):
        import os
        from staticky.utils import native_to_url_path

        self.assertEquals(native_to_url_path(os.path.join("a", "b", "c")), "a/b/c")

    def test_url_to_native(self):
        import os
        from staticky.utils import url_to_native_path

        self.assertEquals(url_to_native_path("a/b/c"), os.path.join("a", "b", "c"))

class WalkTreeTest(unittest.TestCase):
    def _setup_error2(self, listdir, isdir, exc=OSError("Fail")):
        def listdir_side_effect(p):
            if p == os.path.join(".","") or p == os.path.join(".", "a"):
                return ['a']
            return []
        listdir.side_effect = listdir_side_effect

        def is_dir_side_effect(p):
            if p == '.':
                return True
            raise exc
        isdir.side_effect = is_dir_side_effect

    @mock.patch('os.listdir')
    def test_error_reraise1(self, listdir):
        from staticky.utils import walk_tree

        listdir.side_effect = OSError('Fail')
        with self.assertRaises(OSError):
            list(walk_tree(".", onerror=True))

    @mock.patch('os.path.isdir')
    @mock.patch('os.listdir')
    def test_error_reraise2(self, listdir, isdir):
        from staticky.utils import walk_tree

        self._setup_error2(listdir, isdir)
        with self.assertRaises(OSError):
            list(walk_tree(".", onerror=True))

    @mock.patch('os.listdir')
    def test_error_function1(self, listdir):
        from staticky.utils import walk_tree

        exc = OSError('Fail')
        listdir.side_effect = exc

        m = mock.Mock(side_effect=mock.Mock())
        list(walk_tree(".", onerror=m))

        m.side_effect.assert_has_calls([mock.call(exc)])

    @mock.patch('os.path.isdir')
    @mock.patch('os.listdir')
    def test_error_function2(self, listdir, isdir):
        from staticky.utils import walk_tree

        exc = OSError('Fail')
        self._setup_error2(listdir, isdir, exc=exc)

        m = mock.Mock(side_effect=mock.Mock())
        list(walk_tree(".", onerror=m))

        m.side_effect.assert_has_calls([mock.call(exc)])
