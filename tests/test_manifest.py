
try:
    from unittest import mock
except ImportError:
    import mock

try:
    from StringIO import StringIO # python2.7
except ImportError:
    from io import StringIO

import sys
import unittest

if sys.version_info[0] < 3:
    builtin_module = "__builtin__"
else:
    builtin_module = "builtins"

class GlobTest(unittest.TestCase):
    def _make(self, g, trace=False):
        from staticky.manifest import translate_glob
        return translate_glob(g, trace=trace).pattern

    def test_simple(self):
        self.assertEqual(self._make(""), ".*")
        self.assertEqual(self._make("test"), "/test$")

    def test_star(self):
        self.assertEqual(self._make("a*"), r"/a.*?$")
        self.assertEqual(self._make("*b"), r"/.*?b$")
        self.assertEqual(self._make("a*b"), r"/a.*?b$")
        self.assertEqual(self._make("x/*x"), r"^x/.*?x$")
        self.assertEqual(self._make("**"), ".*")

    def test_qmark(self):
        self.assertEqual(self._make("a?"), r"/a.$")
        self.assertEqual(self._make("?b"), r"/.b$")
        self.assertEqual(self._make("a?c"), r"/a.c$")

    def test_slash(self):
        self.assertEqual(self._make("/b"), "^/b$")
        self.assertEqual(self._make("/test"), "^/test$")
        self.assertEqual(self._make("/b/b"), "^/b/b$")
        self.assertEqual(self._make("/b//b"), "^/b/b$")

    def test_slash_stars(self):
        self.assertEqual(self._make("**/b"), "/b$")
        self.assertEqual(self._make("a/**/b"), "^a/.*?/b$")
        self.assertEqual(self._make("c/**"), "^c/")
        self.assertEqual(self._make("/b/**"), "^/b/")

    def test_group(self):
        self.assertEqual(self._make("b[a]b"), "/b[a]b$")
        self.assertEqual(self._make("[abc]"), "/[abc]$")
        self.assertEqual(self._make("[!abc]"), "/[^abc]$")
        self.assertEqual(self._make("[]abc]"), r"/[]abc]$")
        self.assertEqual(self._make("[ab"), r"/\[ab$")

    @mock.patch('sys.stderr', new_callable=StringIO)
    def test_trace(self, mock_stderr):
        self._make("a", trace=True)
        self.assertEqual(mock_stderr.getvalue(), "_state_start 'a'\n_state_main ''\n")

    def test_invalid(self):
        with self.assertRaises(ValueError):
            self._make("**x")

        with self.assertRaises(ValueError):
            self._make("x**")

        with self.assertRaises(ValueError):
            self._make("x/**x")

class ManifestTest(unittest.TestCase):
    def test_manifest_strings(self):
        from staticky.manifest import Manifest

        c = Manifest(["- **", "+ /test"])
        self.assertTrue(c.matches("/test"))
        self.assertFalse(c.matches("/test2"))

    def test_manifest_comments(self):
        from staticky.manifest import Manifest

        c = Manifest(["- **", "# - /test", "", "+ /test"])
        self.assertTrue(c.matches("/test"))
        self.assertFalse(c.matches("/test2"))

    def test_manifest_exc(self):
        from staticky.manifest import Manifest

        with self.assertRaises(TypeError):
            c = Manifest([6])

    def test_manifest_no_prefix(self):
        from staticky.manifest import Manifest

        c = Manifest(["- **", "/test"])
        self.assertTrue(c.matches("/test"))
        self.assertFalse(c.matches("/test2"))

    def test_manifest_tuples(self):
        from staticky.manifest import Manifest

        c = Manifest([("-", "**"), ("+", "/test")])
        self.assertTrue(c.matches("/test"))
        self.assertFalse(c.matches("/test2"))

    def test_manifest_mixed(self):
        from staticky.manifest import Manifest

        c = Manifest([("-", "**"), "+ test2", ("+", "/test")])
        self.assertTrue(c.matches("/test"))
        self.assertTrue(c.matches("/test2"))
        self.assertFalse(c.matches("/test3"))

    def test_manifest_filename(self):
        from staticky.manifest import Manifest

        mm = mock.MagicMock(return_value=StringIO('- /test\n- /test2\n'))
        with mock.patch(builtin_module+'.open', mm):
            c = Manifest("file.txt")
            self.assertFalse(c.matches("/test"))
            self.assertFalse(c.matches("/test2"))
            self.assertTrue(c.matches("/test3"))
            mm.assert_called_with("file.txt", "r")
