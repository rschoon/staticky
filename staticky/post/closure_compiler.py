
import re
import tempfile
import shutil
import subprocess
import sys

class CompressJS(object):
    def __init__(self, closure_jar=None, java="java", keep_bang_comments=True):
        self.closure_jar = closure_jar
        self.java = java
        self.keep_bang_comments = keep_bang_comments

    RE_BANG_COMMENT = re.compile(r"\/\*!(.*?)\*\/".encode('utf-8'), re.S)

    def __call__(self, ctx, path):
        if self.closure_jar is None:
            sys.stderr.write("skipping compress: no closure_jar provided to closure compiler rule\n")
            return

        fullpath = ctx.resolve(path)

        keep_comments = []
        if self.keep_bang_comments:
            with open(fullpath, "rb") as f:
                text = f.read()
                for m in self.RE_BANG_COMMENT.finditer(text):
                    keep_comments.append(m.group(0))

        with tempfile.NamedTemporaryFile() as tmpf:
            subprocess.check_call([self.java, "-jar", self.closure_jar,
                                "--js", fullpath,
                                "--js_output_file", tmpf.name])

            with ctx.open(path, "wb") as f:
                for comment in keep_comments:
                    f.write(comment)
                    f.write(b"\n")

                tmpf.seek(0)
                shutil.copyfileobj(tmpf, f)
