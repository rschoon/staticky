
import subprocess

from ..utils import *

class YUICompressor(object):
    def __init__(self, yui_compressor="yui-compressor", charset=None, type=None):
        self.yui_compressor = yui_compressor
        self.type = type
        self.charset = charset

    def __call__(self, ctx, path):
        fullpath = ctx.resolve(path)
        with InplaceTempFile(fullpath) as f:
            if isinstance(self.yui_compressor, list):
                cmd = list(self.yui_compressor)
            else:
                cmd = [self.yui_compressor]

            if self.type is not None:
                cmd.extend(["--type", self.type])

            if self.charset is not None:
                cmd.extend(["--type", self.charset])

            cmd.append(fullpath)

            subprocess.check_call(cmd, stdout=f)
