
import subprocess
import sys

from ..utils import *

class Yuglify(object):
    def __init__(self, yuglify="yuglify", type=None):
        self.yuglify = yuglify
        self.type = type

    def __call__(self, ctx, path):
        fullpath = ctx.resolve(path)

        try:
            with InplaceTempFile(fullpath, named=True) as fdest:
                if isinstance(self.yuglify, list):
                    cmd = list(self.yuglify)
                else:
                    cmd = [self.yuglify]

                if self.type is not None:
                    cmd.extend(["--type", self.type])
                elif path.endswith(".js"):
                    cmd.extend(["--type", "js"])
                elif path.endswith(".css"):
                    cmd.extend(["--type", "css"])

                cmd.extend(["--terminal"])

                with open(fullpath, "rb") as fsrc:
                    subprocess.check_call(cmd, stdout=fdest, stdin=fsrc)
        except subprocess.CalledProcessError as exc:
            sys.stderr.write("yuglify: command failed with %d, skipping %r\n"%(exc.returncode, fullpath))
