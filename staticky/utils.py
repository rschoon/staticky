
from collections import namedtuple
import errno
import os
import posixpath
import shutil
import subprocess
import sys
import tempfile
import time

__all__ = [
    'closing_iterator', 'getmtime', 'getmtime_recursive', 'InplaceTempFile',
    'iter_file_chunks', 'lookup', 'mkdir_p', 'native_to_url_path',
    'remove_path_first_file', 'url_to_native_path', 'touch_file', 'walk_tree'
]

NS = 1000000000
HAVE_NS_MTIME = sys.version_info >= (3, 3)

def closing_iterator(itr):
    """Call .close() *immediately* upon finishing iteration."""
    try:
        for c in itr:
            yield c
    finally:
        itr.close()

def getmtime(path):
    st = os.stat(path)
    try:
        return st.st_mtime_ns
    except AttributeError:
        return st.st_mtime*NS

def getmtime_recursive(path):
    if os.path.isdir(path):
        return max(getmtime(os.path.join(path, i.path)) for i in walk_tree(path))
    return getmtime(path)

class InplaceTempFile(object):
    def __init__(self, path, named=False, allow_exc=False):
        self.path = path
        self.named = named
        self.allow_exc = allow_exc

    def __enter__(self):
        if self.named:
            self.tempfile = tempfile.NamedTemporaryFile()
        else:
            self.tempfile = tempfile.TemporaryFile()
        self.tempfile.__enter__()
        return self.tempfile

    def __exit__(self, exc, val, tb):
        if exc is None or self.allow_exc:
            self.tempfile.seek(0)
            with open(self.path, "wb") as f:
                shutil.copyfileobj(self.tempfile, f)

        self.tempfile.__exit__(exc, val, tb)

def iter_file_chunks(filelike, chunksize=2**14, close=True):
    try:
        while True:
            chunk = filelike.read(chunksize)
            if len(chunk) == 0:
                break
            yield chunk
    finally:
        if close and hasattr(filelike, "close"):
            filelike.close()

def lookup(name):
    # we only look up classes and functions, but both are callable
    if callable(name):
        return name

    module_name, func_name = name.split(':', 1)
    __import__(module_name)

    module = sys.modules[module_name]
    return getattr(module, func_name)

def mkdir_p(path, force=False):
    if force:
        remove_path_first_file(path)

    try:
        os.makedirs(path)
    except OSError as exc:  # pragma: no cover
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise
    except Exception as exc: # pragma: no cover
        if isinstance(exc, FileExistsError):
            if not os.path.isdir(path):
                raise
            else:
                return
        raise

def remove_path_first_file(path):
    lastpath = subpath = path
    while subpath:
        if os.path.isfile(subpath):
            os.unlink(subpath)
            break
        subpath, basename = os.path.split(subpath)
        if lastpath == subpath:
            break
        lastpath = subpath

if HAVE_NS_MTIME:
    def _touch_file_ns(path, timeval):
        os.utime(path, ns=(timeval, timeval))
else:
    def _touch_file_ns(path, timeval):
        os.utime(path, (timeval/NS, timeval/NS))

def touch_file(path, timeval=None, _inf=float('Inf')):
    if timeval is None or timeval == _inf:
        timeval = time.time()*NS
    _touch_file_ns(path, timeval)

WalkedTreeEntry = namedtuple("WalkedTreeEntry", ("path", "isdir"))

def walk_tree(root, onerror=None):
    """Like os.walk, but return paths relative to root and don't return dirs"""

    paths = [""]
    yield WalkedTreeEntry("", True)

    while len(paths) > 0:
        path = paths.pop(0)
        try:
            subpaths = os.listdir(os.path.join(root, path))
        except OSError as exc:
            if onerror == True:
                raise
            elif onerror is not None:
                onerror(exc)
            continue

        for subpath in subpaths:
            fullsubpath = os.path.join(root, path, subpath)
            try:
                if not os.path.islink(fullsubpath) and os.path.isdir(fullsubpath):
                    paths.append(os.path.join(path, subpath))
                    yield WalkedTreeEntry(os.path.join(path, subpath), True)
                else:
                    yield WalkedTreeEntry(os.path.join(path, subpath), False)
            except OSError as exc:
                if onerror == True:
                    raise
                elif onerror is not None:
                    onerror(exc)

#
# Paths
#

def url_to_native_path(path):
    """Normalize a url path and convert it to a native file system path"""
    return os.path.join(*path.split("/"))

def _split(path):
    rv = []
    while len(path) > 0:
        spl = os.path.split(path)
        rv.insert(0, spl[1])
        path = spl[0]
    return rv

def native_to_url_path(path):
    """Convert a native file system path to a url path"""
    return "/".join(_split(path))
