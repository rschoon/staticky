
import hashlib
import os
import posixpath
import subprocess

from ..utils import *

class Sass(object):
    def __init__(self, cache_dir=None, include=(), sass="sass"):
        self.sass = sass
        self.include = include
        self.cache_dir = cache_dir

    def __call__(self, target, input, output):
        cache_dir = self.cache_dir

        if posixpath.basename(input).startswith("_"):
            return None        # TODO make a settings variable?

        inpath = target.resolve(input)
        if inpath is None:
            return None

        cmd = [self.sass]

        for path in self.include:
            cmd.extend(['-I', target.resolve(path)])
        cmd.extend(['-I', target.resolve(posixpath.dirname(input))])

        if cache_dir is not None and len(cache_dir) > 0:
            cmd.extend(["--cache-location", os.path.join(cache_dir, "cache")])

        outpath = target.resolve(output)
        mkdir_p(os.path.dirname(outpath))

        if not os.path.isfile(outpath):
            cmd.extend([inpath, outpath])
        else:
            cmd.extend(["--update", "%s:%s"%(inpath, outpath, )])

        subprocess.check_call(cmd)

