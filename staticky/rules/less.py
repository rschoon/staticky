
import os
import subprocess

from ..utils import *

class Less(object):
    def __init__(self, include=(), lessc="lessc"):
        self.lessc = lessc
        self.include = include

    def _make_include(self, target, filename):
        yield target.resolve(filename)

        for path in self.include:
            yield target.resolve(path)

    def __call__(self, context, input, output):
        cmd = [
            self.lessc,
            '--include-path=' + ";".join(self._make_include(target, input)),
            context.resolve(output)
        ]

        subprocess.check_call(cmd)
