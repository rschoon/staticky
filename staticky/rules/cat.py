
import os

from ..utils import *

try:
    FileNotFoundError
except OSError:
    FileNotFoundError = OSError

class CatFile(object):
    def __init__(self):
        pass

    def __call__(self, target, input, output):
        try:
            with target.open(input, "r", encoding='utf-8', errors='replace') as catfile:
                paths = (line.strip() for line in catfile if not line.startswith("#"))
                paths = [path for path in paths if len(path) > 0]
        except FileNotFoundError:
            return

        with target.open(output, "wb") as fout:
            for path in paths:
                with target.open(path, "rb") as fin:
                    for chunk in iter_file_chunks(fin):
                        fout.write(chunk)
