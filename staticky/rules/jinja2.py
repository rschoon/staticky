
import os

from jinja2 import Environment, BaseLoader

class ResolvedLoader(BaseLoader):
    def __init__(self, ctx):
        self.ctx = ctx

    def get_source(self, environment, template):
        path = self.ctx.resolve(template)
        mtime = os.path.getmtime(path)
        with open(path, "rb") as f:
            source = f.read().decode('utf-8')
        def _mtime():
            return mtime == os.path.getmtime(path)
        return source, path, _mtime

class Jinja2(object):
    def __init__(self, env_options={}):
        self.env_options = env_options

    def __call__(self, ctx, input, output):
        env_options = self.env_options.copy()
        env_options['loader'] = ResolvedLoader(ctx)

        env = Environment(**env_options)
        tpl = env.get_template(input)

        with ctx.open(output, "wb") as f:
            f.write(tpl.render().encode('utf-8'))
