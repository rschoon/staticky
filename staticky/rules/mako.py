
from __future__ import absolute_import

import os

import mako.lookup
from mako.template import Template as MakoTemplate

class ResolvedTemplateLookup(mako.lookup.TemplateCollection):
    def __init__(self, target, dirs, template_factory):
        self.target = target
        self.dirs = dirs
        self.template_factory = template_factory

    def adjust_uri(self, uri, filename):
        return uri

    def filename_to_uri(self, uri, filename):
        raise NotImplementedError

    def uri_to_filename(self, uri, relativeto=None):
        assert relativeto is None

        if uri.startswith("/"):
            uri = uri[1:]

        for path in self.dirs:
            filename = os.path.join(path, uri)
            if os.path.isfile(filename):
                return filename

        filename = self.target.resolve(uri)
        if filename is not None:
            return filename

    def get_template(self, uri, relativeto=None):
        filename = self.uri_to_filename(uri, relativeto)
        if filename is not None:
            with open(filename, "r") as f:
                return self.template_factory(f.read(), filename=uri, lookup=self)

    def has_template(self, uri):
        return self.uri_to_filename(uri) is not None

class Mako(object):
    def __init__(self, include=(), template_factory=MakoTemplate, data_default={}):
        self.include = include
        self.template_factory = template_factory
        self.data_default = data_default

    def __call__(self, target, input, output):
        lookup = ResolvedTemplateLookup(target, self.include, self.template_factory)
        tpl = lookup.get_template("/%s"%(input,))
        with target.open(output, "w") as f:
            data = {
                'staticky_context' : target
            }
            data.update(self.data_default)
            f.write(tpl.render(**data))
