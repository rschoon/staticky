
import logging
from operator import attrgetter
import os
import posixpath
import shutil
import warnings
import weakref

from .utils import *
from .log import *

class RuleError(RuntimeError):
    def __init__(self, msg, parent=None):
        self.parent = parent
        super(RuleError, self).__init__(msg)

class TargetRegenNeededException(Exception):
    """This is used internally to signal when targets need to be regenerated."""

class CycleWarning(UserWarning):
    pass

try:
    FileNotFoundError
except NameError: # pragma: no cover
    FileNotFoundError = OSError
    NotADirectoryError = OSError
    IsADirectoryError = OSError

class BuildTask(object):
    def __init__(self, job, path, target=None):
        self.target = target
        self.path = path
        self.job = job

    def resolve(self, path):
        if self.target is not None:
            # Normal build target
            return self.target.resolve(path, self.job)

        # if we're in post build
        if not path.startswith("/"):
            path = posixpath.normpath(posixpath.join(posixpath.dirname(self.path), path))

        if path == self.path:
            return _resolve(self.job.workdir_post, path, from_target=self.target)

        return _resolve(job.workdir_build, path, relativeto=self.path, job=job, from_target=self.target)

    def open(self, filename, mode='r', **kwargs):
        resolved_filename = self.resolve(filename)
        mkdir_p(os.path.dirname(resolved_filename))
        return open(resolved_filename, mode, **kwargs)

def _resolve(basepath, path, relativeto=None, job=None, from_target=None):
    if path.startswith("!"):
        return path[1:]

    if not path.startswith("/"):
        if not relativeto:
            raise ValueError("expect relative to if relative path (%r)"%(path,))
        path = posixpath.normpath(posixpath.join(os.path.dirname(relativeto), path))
    else:
        path = posixpath.normpath(path)

    if job is not None and path != relativeto:
        logger.debug("Building %s from %r", path, from_target)
        job.build(path)
    return os.path.join(basepath, url_to_native_path(path[1:]))

class Target(object):
    def __init__(self, path, src, rule):
        self.path = path
        self.src = src
        self.targets = {}
        self.rule = rule

        self._dep_key = ("depends", self.rule.key() if self.rule is not None else None)
        self.depends = None

    def __repr__(self):
        return "<TargetFile(%r, rule=%r)>"%(self.path, self.rule)

    def __str__(self):
        return self.path

    def __hash__(self):
        return hash(self.path)

    def build(self, job):
        if not job.start_build(self):
            return self.resolve(self.path, job)

        if self.depends is None:
            self.depends = job.meta.get(self.path, self._dep_key, None)

        try:
            mtime = self.new_mtime(job)
        except (FileNotFoundError, NotADirectoryError):
            raise TargetRegenNeededException

        destpath = self.resolve(self.path, job)
        exists = os.path.isfile(destpath)

        if job.force or mtime > self.old_mtime(job) or not exists:
            logger.info("Building %s", self)

            src = self.src.path if self.src else None
            build_ctx = BuildTask(job, self.path, target=self)

            try:
                self.rule(build_ctx, src, self.path)
            except Exception as exc:
                try:
                    os.unlink(destpath)
                except OSError:
                    pass

                if not isinstance(exc, RuleError):
                    logger.exception("%r raised an exception on %r", self.rule, src)
                    raise RuleError("%r raised from %r"%(exc, self), exc)
                else:
                    raise

            exists = os.path.isfile(destpath)
            if exists:
                touch_file(destpath, mtime)

            if self.depends is None:
                self.depends = []
            job.meta.set(self.path, self._dep_key, self.depends)

        return self.resolve(self.path, job)

    def new_mtime(self, job, calculated=None):
        if calculated is None:
            calculated = set()

        if self.depends is None:
            # XXX No known dependencies, either meta was clobbered or ?
            logger.debug("Depends is unknown for %s", self)
            return float('inf')

        mtime = self.src.new_mtime(job, calculated)
        for dep in self.depends:
            target = job.targets.get_target(dep)
            if target in calculated:
                continue
            calculated.add(target)

            if target:
                target_mtime = target.new_mtime(job, calculated)
                if target_mtime > mtime:
                    mtime = target_mtime
        return mtime

    def old_mtime(self, job):
        try:
            return getmtime(_resolve(job.workdir_build, self.path, from_target=self))
        except FileNotFoundError:
            return 0

    def resolve(self, path, job):
        if not path.startswith("/"):
            path = posixpath.normpath(posixpath.join(posixpath.dirname(self.path), path))
        if path != self.path:
            if self.depends is None:
                self.depends = []
            self.depends.append(path)
        return _resolve(job.workdir_build, path, relativeto=self.path, job=job, from_target=self)

class TargetSrc(Target):
    def __init__(self, path, srcpath):
        super(TargetSrc, self).__init__(path, None, None)
        self.srcpath = srcpath

    def build(self, job):
        path = self.resolve(self.path, job)
        if os.path.isdir(path) or os.path.isdir(self.srcpath):
            raise TargetRegenNeededException

        try:
            mtime = self.new_mtime(job)
        except (FileNotFoundError, NotADirectoryError):
            raise TargetRegenNeededException

        if not job.start_build(self):
            return path

        if job.force or mtime > self.old_mtime(job) or not os.path.isfile(path):
            logger.info("Building %s", self.path)

            mkdir_p(os.path.dirname(path))
            shutil.copyfile(self.srcpath, path)

            touch_file(path, mtime)

        return path

    def new_mtime(self, job, calculated=None):
        return getmtime(self.srcpath)

class TargetDir(object):
    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return "<TargetDir(%r)>"%(self.path, )

    def __hash__(self):
        return hash(self.path)

    def iter_child_targets(self, job):
        matches = set()
        matches_src = set()

        for target in job.targets.iter_all_targets():
            if self.path == '/' or target.path.startswith(self.path+'/'):
                if isinstance(target, TargetSrc):
                    matches_src.add(target)
                else:
                    matches.add(target)

        # source
        for target in sorted(matches_src, key=attrgetter("path")):
            yield target

        # non-source
        for target in sorted(matches, key=attrgetter("path")):
            yield target

    def build(self, job):
        if not job.start_build(self):
            return _resolve(job.workdir_build, self.path, from_target=self)

        for target in self.iter_child_targets(job):
            target.build(job)

        subpaths = sorted(f.path for f in self.iter_child_targets(job))
        job.meta.set(self.path, "sub-paths", subpaths)

        return _resolve(job.workdir_build, self.path, from_target=self)

    def new_mtime(self, job, calculated=None):
        if calculated is None:
            calculated = set()

        mtime = 0
        for target in self.iter_child_targets(job):
            if target in calculated:
                continue
            calculated.add(target)

            target_mtime = target.new_mtime(job, calculated)
            if target_mtime > mtime:
                mtime = target_mtime

        subpaths = sorted(f.path for f in self.iter_child_targets(job))
        if subpaths != job.meta.get(self.path, "sub-paths", None):
            return float('inf')

        return mtime

#
# Build Job
#

class BuildJob(object):
    def __init__(self, targets, workdir, force=False):
        self.targets = targets
        self.workdir = workdir
        self.built = set()
        self.force = force

    def start_build(self, target):
        if target in self.built:
            return False
        else:
            self.built.add(target)
        return True

    @property
    def workdir_build(self):
        return os.path.join(self.workdir, "build")

    @property
    def workdir_post(self):
        return os.path.join(self.workdir, "post")

    @property
    def meta(self):
        return self.targets.meta

    def _build(self, path):
        target = self.targets.get_target(path)
        if target is not None:
            return target.build(self)
        else:
            logger.debug("Could not build %r, no matching rule.", path)

    def build(self, path):
        if not path.startswith("/"):
            path = "/" + path

        try:
            return self._build(path)
        except TargetRegenNeededException:
            # target's src could have moved, or it really is gone
            logger.debug("TargetRegen triggered while building %s", path)
            self.targets.update_targets()
            self.reset()

            try:
                return self._build(path)
            except TargetRegenNeededException:
                return None

    def build_post(self, path):
        rules_matching = []
        for rule in self.targets.postrules:
            if rule.match(path):
                rules_matching.append(rule)

        if len(rules_matching) == 0:
            return None

        # XXX We assume file is built (fix for multiprocess?)
        srcpath = _resolve(self.workdir_build, path)
        srcmtime = getmtime(srcpath)

        destpath = _resolve(self.workdir_post, path)
        if os.path.isdir(destpath):
            shutil.rmtree(destpath)

        try:
            destmtime = getmtime(destpath)
        except FileNotFoundError:
            destmtime = 0

        if destmtime >= srcmtime:
            return destpath

        mkdir_p(os.path.dirname(destpath))
        shutil.copyfile(srcpath, destpath)
        post_ctx = BuildTask(self, path)
        for rule in rules_matching:
            try:
                rule(post_ctx, path)
            except Exception as exc:
                if not isinstance(exc, RuleError):
                    logger.exception("%r raised an exception on %r", rule, path)
                    raise RuleError("%r raised from %r"%(exc, self), exc)
                else:
                    raise

        return destpath

    def clean(self, path):
        """TODO: testme"""

        logger.info("Cleaning %s", path)

        fullpath = _resolve(self.workdir_build, path)
        if os.path.isdir(fullpath):
            shutil.rmtree(fullpath)
        else:
            remove_path_first_file(fullpath)

    def reset(self):
        self.built.clear()

#
# Target Collection
#

def _register(list_, entry):
    for i, ei in enumerate(list_):
        if ei.priority < entry.priority:
            list_.insert(i, entry)
            return
    list_.append(entry)

class TargetCollection(object):
    def __init__(self, input, meta, create_job):
        self.input = input

        self.rules = []
        self.postrules = []

        self._dirs = {}
        self._src = {}
        self._targets = None

        self.meta = meta
        self.create_job = create_job

    def log_debug_targets(self):
        if self._targets is None:
            self.update_targets()

        logger.debug("Rules:")
        for rule in self.rules:
            logger.debug("%r", rule)

        logger.debug("Build:")
        for path in sorted(self._targets):
            logger.debug("%r", self._targets[path])

    def register_post(self, entry):
        _register(self.postrules, entry)

    def register(self, entry):
        _register(self.rules, entry)

    def get_target(self, path):
        if self._targets is None:
            self.update_targets()

        if path in self._dirs:
            return self._dirs[path]

        target = self._targets.get(path)
        if target is None:
            # targets could be out of date
            self.update_targets()
            target = self._targets.get(path)
        return target

    def _update_targets_for(self, src, depth=0):
        if depth > 10:
            warnings.warn("Depends cycle possibly found"\
                          "while updating %r (depth=%d)"%(src, depth),
                          CycleWarning)
            return

        for rule in self.rules:
            dest = rule.match(src.path)
            if dest is None:
                continue

            self._targets.pop(dest, None)

            target = Target(dest, src, rule)

            src.targets[dest] = target
            self._targets[dest] = target

            self._update_targets_for(target, depth=depth+1)

    def update_targets(self):
        logger.debug("Regenerating targets.")

        dirs = {}
        new = {}
        missing = set(self._src)
        missing_dirs = set(self._dirs)
        for inp in self.input:
            for fpth in walk_tree(inp):
                path = "/"+native_to_url_path(fpth.path)
                if fpth.isdir:
                    dirs[path] = TargetDir(path)
                    missing_dirs.discard(path)
                else:
                    fullpath = os.path.join(inp, path[1:])
                    if path not in self._src or self._src[path].srcpath != fullpath:
                        new[path] = TargetSrc(path, fullpath)
                    elif path in missing:
                        missing.discard(path)

        self._dirs = dirs

        if missing_dirs or missing:
            # Rules don't need to list every output, so we need to make
            # sure we get those alternate outputs too.
            # TODO: delete missing_dirs and missing and anything we don't
            # know about
            job = self.create_job()
            for path in (job.workdir_build, job.workdir_post):
                try:
                    shutil.rmtree(path)
                except FileNotFoundError:
                    pass

            for path in missing:
                self._src.pop(path, None)

        # add new targets
        for item in new.values():
            self._src[item.path] = item

        if not self._targets or (new or missing_dirs or missing):
            # rebuild target list
            self._targets = {}
            for path, target in self._src.items():
                self._targets[path] = target
                self._update_targets_for(target)

    def iter_all_targets(self):
        if self._targets is None:
            self.update_targets()

        return iter(self._targets.values())

    def iter_all_paths(self):
        self.update_targets()

        for target in sorted(self._targets.values(), key=attrgetter("path")):
            yield target.path
