
import gzip as _gzip
import io
import mimetypes
from operator import attrgetter
import posixpath
import os
import re
import shutil
import sys
import tempfile

from webob import Response as WebObResponse, Request as WebObRequest
from webob.exc import HTTPNotFound

from .build import BuildJob, TargetCollection, RuleError
from .utils import *
from .manifest import Manifest

DEFAULT_META = "staticky.meta.sqlite:MetaData"

try:
    FileNotFoundError
except NameError: # pragma: no cover
    FileNotFoundError = OSError
    NotADirectoryError = OSError
    IsADirectoryError = OSError

#
# Response
#

class StaticIterator(object):
    CHUNKSIZE = 2**14

    def __init__(self, fileobj):
        self.fileobj = fileobj

    def __iter__(self):
        return iter_file_chunks(self.fileobj, self.CHUNKSIZE)

    def accelerate(self, environ):
        if "wsgi.file_wrapper" in environ:
            return environ['wsgi.file_wrapper'](self.fileobj, self.CHUNKSIZE)
        else:
            return self

    @property
    def last_modified(self):
        try:
            fd = self.fileobj.fileno()
        except AttributeError:
            return None
        return os.fstat(fd).st_mtime

    def close(self):
        try:
            self.fileobj.close()
        except: # pragma: no cover
            pass
        self.fileobj = None

class Response(WebObResponse):
    def __init__(self, iterator, path):
        content_type, charset = mimetypes.guess_type(path)
        if content_type is None:
            # XXX TODO content-type should be omitted
            content_type = "application/octet-stream"

        super(Response, self).__init__(app_iter=iterator, content_type=content_type)

        if isinstance(iterator, StaticIterator):
            try:
                self.last_modified = iterator.last_modified
            except OSError:
                pass

    def __call__(self, environ, start_response):
        iterobj = self.conditional_response_app(environ, start_response)
        if isinstance(iterobj, StaticIterator):
            return iterobj.accelerate(environ)
        return iterobj

class StaticFile(object):
    def __init__(self, fileobj, metadata):
        self._fileobj = fileobj
        self._metadata = metadata

    def __getattr__(self, name):
        return getattr(self._fileobj, name)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        return self._fileobj.__exit__(type, value, traceback)

    def __iter__(self):
        return iter(self._fileobj)

    def fileno(self):
        return self._fileobj.fileno()

io.IOBase.register(StaticFile)

#
# Static Registration
#

class StaticFunc(object):
    def __init__(self, rule, kwargs):
        self._func = None
        self.rule = rule
        self.kwargs = kwargs

    @property
    def name(self):
        if isinstance(self.rule, str):
            return self.rule
        return self.rule.__name__

    def _init(self):
        cls = self.rule
        if isinstance(cls, str):
            cls = lookup(cls)
        return cls(**self.kwargs)

    def __call__(self, *args, **kwargs):
        if self._func is None:
            self._func = self._init()
        return self._func(*args, **kwargs)

class StaticRule(object):
    RE_FMT = re.compile(r'{(\w+)}')

    def __init__(self, rule, args, input, output, priority):
        self._func = StaticFunc(rule, args)

        if input.startswith("/") != output.startswith("/"):
            raise ValueError("Both input (%r) and output (%r) must be absolute, or neither."%(input, output))

        self.input_re, self.input_fmt = self._mk_re_fmt(input)
        self.output_re, self.output_fmt = self._mk_re_fmt(output)

        self.priority = priority

    def __repr__(self):
        return "StaticRule(%r, %r, %r, %r, %d)"%(
            self._func.name,
            self._func.kwargs,
            self.input_fmt,
            self.output_fmt,
            self.priority
        )

    def key(self):
        return (self.input_fmt, self.output_fmt, str(self._func.rule))

    def match(self, path):
        if self.input_re is None:
            return path
        elif not self.input_fmt.startswith("/"):
            m = self.input_re.match(os.path.basename(path))
            if m is not None:
                return posixpath.join(posixpath.dirname(path), self.output_fmt%(m.groupdict()))
        else:
            m = self.input_re.match(path)
            if m is not None:
                return self.output_fmt%(m.groupdict())

    def __call__(self, *args, **kwargs):
        return self._func(*args, **kwargs)

    def _mk_re_fmt(self, s):
        if s is None:
            return None, None

        regex = []
        fmt = []

        last = 0
        for m in self.RE_FMT.finditer(s):
            regex.append(re.escape(s[last:m.start()]))
            fmt.append(s[last:m.start()])

            regex.append('(?P<%s>.*?)'%m.group(1))
            fmt.append("%%(%s)s"%m.group(1))

            last = m.end()

        regex.append(re.escape(s[last:]))
        fmt.append(s[last:])

        return re.compile('^%s$'%"".join(regex)), "".join(fmt)

class StaticFiles(object):
    def __init__(self, input, workdir=None, pathvar=None,
                 manifest=None, index="index.html", meta_class=DEFAULT_META):
        """Create a StaticFiles object, which defines a staticky environment.

        The location of source files indicated by ``input``, and may either
        be a path, or a list of a paths.  If a list is given, priority
        increases with later paths.

        The staging area is specified by ``workdir``, and if it is omitted
        or ``None``, a temporary directory will be used that will be cleaned
        up on destruction of the object.

        If ``manifest`` is provided, it controls what files are visible via
        :meth:`~staticky.StaticFiles.open()`, :meth:`~staticky.StaticFiles.as_wsgi()`,
        :meth:`~staticky.StaticFiles.as_view()` or :meth:`~staticky.StaticFiles.install()`,
        although :meth:`~staticky.StaticFiles.install()` can override the
        manifest specified here.  For information on the expected format, see the
        :ref:`Manifest <manifest>` documentation.

        The ``index`` keyword, which defaults to "index.html" may be used to
        indicate which file should be used if a directory (with a trailing slash)
        is requested when using :meth:`~staticky.StaticFiles.as_wsgi()` or
        :meth:`~staticky.StaticFiles.as_view()`.
        """

        if isinstance(input, str):
            input = (input, )
        if not hasattr(input, "__iter__"):
            raise TypeError("input needs to be iterable (got %r)"%(type(input),))
        self.input = input
        self.pathvar = pathvar
        self.manifest = Manifest(manifest)
        self.index = index

        if workdir is None:
            self.workdir = self._tempdir = tempfile.mkdtemp()
            self._shutil = shutil
        else:
            mkdir_p(workdir)
            self.workdir = workdir
            self._tempdir = None

        meta_class = lookup(meta_class)
        meta = meta_class(self)

        self.targets = TargetCollection(self.input, meta, self.create_job)

    def log_debug_targets(self):
        self.targets.log_debug_targets()

    def cleanup(self):
        """Clean up any temporary files and closes any opened resources.

        This method is automatically called on object destruction."""

        if hasattr(self, "meta"):
            self.meta.close()

        if getattr(self, "_tempdir", None) is not None:
            self._shutil.rmtree(self._tempdir)
            self._tempdir = self._shutil = None

    def __del__(self):
        self.cleanup()

    #
    # As Request
    #

    def __call__(self, request, path=None):
        return self.as_view(request, path=path)

    def as_wsgi(self, environ, start_response):
        """Call as a wsgi application.

        The bound method may be passed to anything expecting a
        wsgi application, eg ``make_server('', 8080, staticfiles.as_wsgi)``.
        """

        response = self.as_view(WebObRequest(environ))
        if not response:
            response = HTTPNotFound()
        return response(environ, start_response)

    def as_view(self, request, path=None):
        """Call as a view, expecting a ``WebOb Request`` object ``request``
        and returning a ``WebOb Response`` object.
        """

        if path is None:
            if self.pathvar is not None:
                path = request.urlvars[self.pathvar]
            else:
                path = request.path_info

        if path.endswith("/"):
            if self.index is not None:
                path = path + self.index
            else:
                return HTTPNotFound()

        if not path.startswith("/"):
            path = "/" + path
        else:
            while path.startswith("//"):
                path = path[1:]

        if not self.manifest.matches(path):
            return

        try:
            fh = self.open(path, "rb")
        except (FileNotFoundError, NotADirectoryError, IsADirectoryError):
            return

        return Response(StaticIterator(fh), path)

    #
    # Setup
    #

    def register(self, rule, args={}, input=None, output=None):
        """Register a rule, where ``rule`` is either a callable
        or a string in form of ``MODULE:CLASS``.

        If ``rule`` is a string representing a class, then ``args``
        will be passed to it when instantiated as keyword arguments,
        allowing for rule-specific configuration.  It is expected that
        this class is itself callable, and will be called to build files
        the rule is register for.

        If ``rule`` is already a callable, it will be used directly as the
        callable for building files.  For more information about existing
        rules and the construction of new rules see the :ref:`Rules <rules>`
        documentation.

        ``input`` and ``output`` are strings that describe what filenames
        the rule matches.  The wildcard parts of the url are indicated
        using curly brackets (e.g. ``"{filename}.ext"``) and the matchable
        text can be changed by appending a colon and a regular expression
        (e.g. ``"{filename:[a-z]}.ext"``).  Wildcard names must be included
        in both the ``input`` and the ``output`` strings, as this determines
        how names are mapped.  For example, if ``input`` is
        ``"{filename}.in"`` and ``output`` is ``"{filename}.out"``, a file
        named hello.out would be generated if a filename called hello.in is
        found.
        """

        # TODO Is this still supported? add tests and doc if it is
        if (input is None) != (output is None):
            raise ValueError("input and output must both be included or neither")

        entry = StaticRule(rule, args, input, output, 0)
        return self.targets.register(entry)

    def register_post(self, rule, args={}, name="{f}", priority=0):
        """Register a post-processing rule, where ``rule`` follows the
        same form as used with :meth:`~staticky.StaticFiles.register()`.

        Unlike the :meth:`~staticky.StaticFiles.register()` method, the
        input and output are implicitly the same, and are specified via
        ``name``, and the built file is a transformation of an existing
        file.

        `priority` determines what order post-processing rules are
        evaluated in, so that multiple post-processing rules can be
        chained on files in a consistent order.
        """

        entry = StaticRule(rule, args, name, name, priority)
        return self.targets.register_post(entry)

    #
    # Building
    #

    def create_job(self, force=False):
        return BuildJob(self.targets, self.workdir, force=force)

    def open(self, path, mode="r", force=False, include_post=False, job=None):
        """Opens a built file for reading or writing.

        If the contents of a file are overwritten, changes are likely
        to be lost the next time the file is built, such as if
        modifications are detected in a source file.
        """

        if job is None:
            job = self.create_job(force=force)

        try:
            filename = job.build(path)
        except RuleError:
            filename = None

        if filename is None:
            raise FileNotFoundError(path)

        if include_post:
            filename2 = job.build_post(path)
            if filename2 is not None:
                return open(filename2, mode)
        return StaticFile(open(filename, mode), {})

    def install(self, destdir, gzip=False, clean=False, include_post=True, manifest=None):
        """Copy the built tree of files to ``destdir``.  If a ``manifest``
        provided here, or when the ``StaticFiles`` object was constructed,
        it will be used to control what files are actually copied over.

        If the ``gzip`` flag is provided, then each files generated will
        also be gzip compressed with .gz appended to each filename.

        If the ``clean`` flag is provided, then the destination directory
        will be wiped before installation occurs.

        Any post-processing rules will be called, unless ``include_post``
        is provided with a false value.
        """

        manifest = Manifest(manifest) if manifest else self.manifest

        if clean and os.path.isdir(destdir):
            for path in os.listdir(destdir):
                fullpath = os.path.join(destdir, path)
                if os.path.isdir(fullpath):
                    shutil.rmtree(fullpath)
                else:
                    os.unlink(fullpath)

        job = self.create_job()
        for path in self.targets.iter_all_paths():
            if not manifest.matches(path):
                continue

            destpath = os.path.join(destdir, url_to_native_path(path[1:]))
            mkdir_p(os.path.dirname(destpath), force=True)

            fin = gz = None
            try:
                try:
                    fin = self.open(path, "rb", include_post=include_post, job=job)
                except FileNotFoundError as exc:
                    continue

                if gzip:
                    gz = _gzip.GzipFile(destpath+".gz", "wb")

                with open(destpath, "wb") as fout:
                    for chunk in iter_file_chunks(fin, close=False):
                        fout.write(chunk)
                        if gz is not None:
                            gz.write(chunk)
            finally:
                if fin is not None:
                    fin.close()
                if gz is not None:
                    gz.close()
