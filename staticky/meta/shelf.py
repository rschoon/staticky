
from contextlib import closing
import os
import shelve

_DEFAULT = object()

class MetaData(object):
    def __init__(self, sf):
        self.shelf_path = os.path.join(sf.workdir, "metadata.shelf")

    def _shelf(self):
        return closing(shelve.open(self.shelf_path))

    def get(self, path, key, default=_DEFAULT):
        with self._shelf() as shelf:
            try:
                return shelf[path][key]
            except KeyError:
                if default is not _DEFAULT:
                    return default
                raise

    def set(self, path, key, value):
        with self._shelf() as shelf:
            try:
                v = shelf[path]
            except KeyError:
                v = {}
            v[key] = value
            shelf[path] = v

    def close(self):
        pass
