
from contextlib import closing
import json
import os
import sqlite3
import threading

_DEFAULT = object()

def _db_setup(cursor):
    cursor.execute("""SELECT name FROM sqlite_master
                      WHERE type='table' AND name='meta1';""")
    if cursor.fetchone() is None:
        cursor.execute("""CREATE TABLE meta1
                        (path text, key text, value text);""")
        cursor.execute("""CREATE UNIQUE INDEX meta1_key ON meta1
                        (path, key);""")

class CursorWrapper(object):
    def __init__(self, path):
        self.db = sqlite3.connect(path)

    def __enter__(self):
        self.cursor = self.db.cursor()
        _db_setup(self.cursor)
        return self.cursor

    def __exit__(self, exc, val, tb):
        self.db.commit()
        self.cursor.close()
        self.db.close()

class MetaData(object):
    def __init__(self, sf):
        self._dbpath = os.path.join(sf.workdir, "metadata.db")

    def get(self, path, key, default=_DEFAULT):
        with CursorWrapper(self._dbpath) as cursor:
            keystr = json.dumps(key)
            cursor.execute("""SELECT value FROM meta1
                            WHERE path = ? AND key = ?""",
                            (path, keystr))
            val = cursor.fetchone()
            if val is None:
                if default is _DEFAULT:
                    raise KeyError("%s:%s"%(path, keystr))
                return default
            return json.loads(val[0])

    def set(self, path, key, value):
        with CursorWrapper(self._dbpath) as cursor:
            keystr = json.dumps(key)
            valuestr = json.dumps(value)
            cursor.execute("""INSERT OR REPLACE INTO meta1
                            (path, key, value) VALUES
                            (?, ?, ?)""", (path, keystr, valuestr))

    def close(self):
        pass
