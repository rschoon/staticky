
import re
import sys

RE_SPLIT_STR = re.compile(r'^(\+|-)\s+(.*?)$')

class ManifestRule(object):
    def __init__(self, rule, exclude=False):
        self.exclude = exclude
        self.regex = translate_glob(rule)

    def __repr__(self):
        return "<ManifestRule(%r, exclude=%r)>"%(self.regex.pattern, self.exclude)

    def matches(self, path):
        m = self.regex.search(path)
        if m:
            return not self.exclude

class Manifest(object):
    def __init__(self, manifest):
        self._rules = []
        if manifest is not None:
            self.parse(manifest)

    def parse(self, manifest):
        fileobj = None
        if isinstance(manifest, str):
            manifest = fileobj = open(manifest, "r")

        try:
            for rule in manifest:
                if isinstance(rule, str):
                    rule = rule.rstrip()
                    if rule.startswith("#") or len(rule) == 0:
                        continue

                    m = RE_SPLIT_STR.match(rule)
                    if m is not None:
                        rule = (m.group(1), m.group(2))
                    else:
                        rule = ('+', rule)

                if isinstance(rule, tuple):
                    rule = ManifestRule(rule[1], exclude=rule[0] == '-')

                if not isinstance(rule, ManifestRule):
                    raise TypeError("Got unexpected rule value %r"%(rule,))

                self._rules.append(rule)
        finally:
            if fileobj:
                fileobj.close()

    def matches(self, path):
        if not path.startswith("/"):
            path = "/" + path

        v = True
        for r in self:
            m = r.matches(path)
            if m is not None:
                v = m
        return v

    def __iter__(self):
        return iter(self._rules)

#
# Translate
#

class _TranslateGlob(object):
    def __init__(self, s, trace):
        self.s = s
        self.regex = []
        self.state = self._state_start
        self.i = 0
        self.n = len(s)

        self.match_all = False
        self.has_slash = False
        self.leading_starslash = False
        self.trailing_starslash = False

        while self.i < self.n:
            c = self.s[self.i]
            if trace:
                sys.stderr.write("%s %r\n"%(self.state.__name__, c))
            self.i += 1
            self.state(c)

        if trace:
            sys.stderr.write("%s %r\n"%(self.state.__name__, ''))
        self.state('')

    def _state_start(self, c):
        if c == '':
            self.match_all = True
        elif c == '*':
            self.state = self._state_prefix_star
        elif c == '[':
            self.state = self._state_bracket
        elif c == '?':
            self.regex.append('.')
            self.state = self._state_main
        elif c == '/':
            self.regex.append('/')
            self.has_slash = True
            self.state = self._state_main
        else:
            self.regex.append(c)
            self.state = self._state_main

    def _state_prefix_star(self, c):
        if c == '*':
            self.state = self._state_prefix_star2
        else:
            self.regex.append(".*?")
            self.i -= 1
            self.state = self._state_main

    def _state_prefix_star2(self, c):
        if c == '/':
            self.has_slash = True
            self.leading_starslash = True
            self.state = self._state_main
        elif c == '':
            self.match_all = True
        else:
            raise ValueError

    def _state_main(self, c):
        if c == '':
            pass
        elif c == '*':
            self.state = self._state_star
        elif c == '?':
            self.regex.append('.')
        elif c == '/':
            self.state = self._state_slash
        elif c == '[':
            self.state = self._state_bracket
        else:
            self.regex.append(re.escape(c))

    def _state_star(self, c):
        if c == '*':
            raise ValueError
        self.regex.append(".*?")
        self.i -= 1
        self.state = self._state_main

    def _state_slash(self, c):
        self.has_slash = True
        if c == '/':
            return
        elif c == '*':
            self.state = self._state_slashstar
        else:
            self.regex.append("/")
            self.i -= 1
            self.state = self._state_main

    def _state_slashstar(self, c):
        if c == '*':
            self.state = self._state_slashstarstar
        else:
            self.regex.append("/.*?")
            self.regex.append(c)
            self.state = self._state_main

    def _state_slashstarstar(self, c):
        if c == '':
            self.trailing_starslash = True
        elif c == '/':
            self.regex.append("/.*?/")
            self.state = self._state_main
        else:
            raise ValueError

    def _state_bracket(self, c):
        self.bracket = [c]
        self.state = self._state_bracket_main

    def _state_bracket_main(self, c):
        if c == ']':
            if self.bracket[0] == '!':
                prefix = "^"
                series = "".join(self.bracket[1:])
            else:
                prefix = ""
                series = "".join(self.bracket)

            self.regex.append("[%s%s]"%(prefix, series.replace("\\", "\\\\")))

            self.state = self._state_main
        elif c == '':
            self.regex.append(re.escape("["+"".join(self.bracket)))
        else:
            self.bracket.append(c)

    def compile(self):
        if self.match_all:
            return re.compile('.*')

        if not self.has_slash or self.leading_starslash:
            self.regex.insert(0, "/")
        else:
            self.regex.insert(0, "^")

        if self.trailing_starslash:
            self.regex.append("/")
        else:
            self.regex.append("$")

        return re.compile("".join(self.regex))

def translate_glob(s, trace=False):
    return _TranslateGlob(s, trace).compile()
