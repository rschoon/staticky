
Changes
=======

0.2 (TBD)
---------

* Rewrite building process, so that outputs can be used as inputs for
  other rules.
* Add manifest specification to describe what files to include as
  installed/served output
* Introduce rule for Jinja templates.
* Introduce rule for yuglify compressor.
* Split existing compressors into own rules.
* Introduce some basic tests and documentation.

0.1.3 (Jun 13 2014)
-------------------

* Make paths passed to rules more consistent.
* Packaging updates.

0.1.2 (Jun 5 2014)
------------------

* Sanitize urls.
* Introduce simple LESS rule.

0.1.1 (Jun 4 2014)
------------------

* Packaging fixes.

0.1 (Jun 4 2014)
----------------

* First initial release.
