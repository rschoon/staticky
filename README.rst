
staticky
========

Overview
--------

staticky is a library that handles compiling collections of "static"
files, such as for webapps.

It takes a set of directories, and a set of rules, and uses them to
generate a set of output files, which can be exposed through several
interfaces such as a wsgi application, or be installed to a target
destination.

It has a few design goals:

* Be able to pull files from several different locations, to allow
  imported static files to be laid out modularly.
* Allow for the creation of custom third party rules.

Future planned improvements also include:

* Integration into web frameworks such as Django or Flask.
* Suitability for generating entirely static sites.
* Improved multiprocess behavior (probably using file locking)

.. WARNING::
    staticky is alpha software, and its API is subject to
    change from release to release!

Quick Example
-------------

Registration::

    from staticky import StaticFiles

    staticfiles = StaticFiles(input=["path/to/static"])

    staticfiles.register('staticky.rules.sass:Sass', input='{name}.scss', output='{name}.css')
    staticfiles.register('staticky.rules.sass:Sass', input='{name}.sass', output='{name}.css')

    staticfiles.register_post('staticky.post.uglify:UglifyJS', name='{name}.js')
    staticfiles.register_post('staticky.post.uglify:UglifyCSS', name='{name}.css')

As a View::

    from webob import Request

    request = webob.Request.blank("/file.css")
    response = staticfiles.as_view(request)

As a WSGI App::

    application = staticfiles.as_wsgi

Pre-Compile (for deployment)::

    staticfiles.install("/var/www/htdocs")
