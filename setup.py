
import os
import re
from setuptools import setup, find_packages

def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename)) as f:
        return f.read()

def read_version(filename):
    return re.search(r"__version__ = '(.*?)'", read(filename)).group(1)

setup(
    name = 'staticky',
    version = read_version('staticky/__init__.py'),
    description = 'Simple static files handling',
    long_description = read("README.rst"),
    author = 'Robin Schoonover',
    author_email = 'robin@cornhooves.org',
    url = "http://bitbucket.org/rschoon/staticky",
    license = 'MIT',
    install_requires = ['WebOb>=1.2.3'],
    packages = find_packages('.'),
    classifiers = [
        "Development Status :: 3 - Alpha",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "Topic :: Internet :: WWW/HTTP :: WSGI",
    ],
)
