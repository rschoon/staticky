
.. _manifest:

Manifest
========

The manifest describes what files are copied by the 
:meth:`~staticky.StaticFiles.install` method, or is visible via the
:meth:`~staticky.StaticFiles.as_view` and :meth:`~staticky.StaticFiles.as_wsgi`
methods.

Manifest Rules
--------------

A manifest is an ordered collection of rules.  The last matching rule
determines if the file is to be included, and each rule either
includes matching files, or excludes matching files.

The determination of whether a rule includes or excludes paths is whether
the type character is '+' or '-'.  The determination of whether a rule 
matches a path is given by a shell-like glob, in the following format:

=============== ==================================================
Pattern         Description
=============== ==================================================
``*``             Matches everything.
``**/``           Subdirectory matching
``/**``           Parent directory matching
``?``             Matches a single character.
``[chars]``       Matches any character in *chars*
``[!chars]``      Matches any character not in *chars*
=============== ==================================================

If a pattern does not contain a slash, then the pattern will only 
be checked against the file's name relative to its directory.  If
is not, then a full path will need to be specified, or use of the
``**`` directory matching will need to be used.

When a manifest is read, it goes through several transformations,
and each of the resulting formats may be used for the manifest.

The first, is as a filename, in which case the file will be opened
and used as a sequence of strings.

The second, a sequence of strings, is converted to a sequence of
tuples.  Each string that is blank or starts with # is ignored,
and all other strings are expected to be formatted as
"type pattern", where type is the type character, and pattern is
the shell-like glob pattern.  For example, ``"+ *.txt"``.

The final format, before being converted into its internal format,
is a list of tuples in the form of (typecharacter, pattern).  For
example, ``('+', '*.txt')``.
