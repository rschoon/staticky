
import sys, os
import re

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from staticky import __version__ as release

extensions = [
    "sphinx.ext.autodoc",
]

source_suffix = '.rst'
master_doc = 'index'

project = 'staticky'
copyright = '2014, Robin Schoonover'

autoclass_content = 'both'
