
Creating New Rules
==================

Rules are simple callable classes.  Their constructors take
any parameters as named keywords.  They are invoked as a
callable to actually perform the rule operation using the
following parameters:
* context
* input file
* output file

Context contains several useful methods, including:

.. function:: open(path, mode)

    Open a file.  If it does not exist, it will be generated if
    any rule is capable of generating it, and if it is not
    currently being generated.

    This is a shortcut for calling the builtin open() with
    resolve()'s return value.


.. function:: resolve(name)

    Return the real path on the filesystem for a path. If it
    does not exist, it will be generated if any rule is capable
    of generating it, and if it is not currently being generated.
