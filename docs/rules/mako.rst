
Mako
====

Usage::

    staticfiles.register('staticfiles.rules.mako:Mako', input='{name}.mako', output='{name}.html', **params)

This calls mako to produce HTML (or other files) from Mako templates.

Parameters:

* include
* template_factory
