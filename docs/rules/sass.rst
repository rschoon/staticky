
Sass
====

Usage::

    staticfiles.register('staticfiles.rules.sass:Sass', input='{name}.{ext:sass|scss}', output='{name}.css', **params)

This rule runs sass to produce CSS files from sass or scss files.

Parameters:

* cache_dir
* include
* sass
