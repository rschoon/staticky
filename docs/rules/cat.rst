
Cat
===

Usage::

    staticfiles.register('staticfiles.rules.cat:Cat', input='{name}.{ext}.cat', output='{name}.{ext}')

This rule concatenates several additional files together from the
contents of a singular input file, where each file to be concatenated
is each provided on their own line in order.  Any line that is blank
or starts with # is ignored.

This rule takes no parameters.
