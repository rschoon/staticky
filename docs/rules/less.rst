
Less
====

Usage::

    staticfiles.register('staticfiles.rules.less:Less', input='{name}.less', output='{name}.css', **params)

This runs lessc to produce CSS files from LESS files.

Parameters:
 
* include
* lessc
